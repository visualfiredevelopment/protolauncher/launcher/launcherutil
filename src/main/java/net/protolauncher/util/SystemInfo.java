package net.protolauncher.util;

public class SystemInfo {

    // Constants
    public static final String OS_NAME;
    public static final String OS_VERSION;
    public static final String OS_ARCH;
    public static final String OS_BIT;
    public static final String USER_HOME;

    // Static Initializer
    static {
        OS_NAME = getSystemPlatform();
        OS_VERSION = getSystemVersion();
        OS_ARCH = getSystemArchitecture();
        OS_BIT = OS_ARCH.replace("x84", "32").replace("x64", "64");
        USER_HOME = getUserHome();
    }

    // Suppress default constructor
    private SystemInfo() {}

    // Determines the Mojang-Based string to use to determine the platform
    private static String getSystemPlatform() {
        final String name = System.getProperty("os.name").toLowerCase();
        if (name.contains("win")) {
            return "windows";
        } else if (name.contains("mac")) {
            return "osx";
        } else if (name.contains("nix") || name.contains("nux") || name.contains("aix")) {
            return "linux";
        } else {
            return "other";
        }
    }

    // Straight up get the version property
    private static String getSystemVersion() {
        return System.getProperty("os.version");
    }

    // Determine whether the architecture is 64-bit or 32-bit
    private static String getSystemArchitecture() {
        final String procArch = System.getenv("PROCESSOR_ARCHITECTURE");
        final String wow64Arch = System.getenv("PROCESSOR_ARCHITEW6432");
        if (procArch != null && procArch.endsWith("64") || wow64Arch != null && wow64Arch.endsWith("64")) {
            return "x64";
        } else {
            return "x86";
        }
    }

    // Fetches the user home and appends "desktop" if windows
    private static String getUserHome() {
        String home = System.getProperty("user.home");
        if (OS_NAME.equals("windows")) {
            home += "\\Desktop";
        }
        return home;
    }

}
