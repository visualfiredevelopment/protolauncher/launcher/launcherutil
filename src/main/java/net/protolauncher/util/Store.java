package net.protolauncher.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Type;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Store<T extends IStorable> extends LinkedHashMap<String, T> implements ISaveable, Iterable<T> {

    // GSON & Location
    private transient final Gson gson;
    private transient final Type type;
    private transient Path location;

    /**
     * Constructs a new store.
     *
     * @param gson The gson to use for saving and loading.
     * @param location The location of the JSON file.
     * @param generic The generic that this instance of this class is using (due to type erasure).
     */
    public Store(Gson gson, Path location, Class<T> generic) {
        this.gson = gson;
        this.location = location;
        this.type = TypeToken.getParameterized(Store.class, generic).getType();
    }

    // Putting
    public Store<T> put(T value) {
        this.put(value.getUuid(), value);
        return this;
    }
    @SafeVarargs
    public final Store<T> putAll(T... values) {
        for (T value : values) {
            this.put(value.getUuid(), value);
        }
        return this;
    }

    // Replacing
    public Store<T> replace(T value) {
        this.replace(value.getUuid(), value);
        return this;
    }

    // Removing
    public Store<T> remove(T value) {
        this.remove(value.getUuid());
        return this;
    }

    // Implement ISaveable
    @Override
    public Gson getGson() {
        return gson;
    }
    @Override
    public Path getLocation() {
        return location;
    }
    @Override
    public Type getType() {
        return type;
    }

    // Setters
    public void setLocation(Path location) {
        this.location = location;
    }

    // Implement Iterable
    @NotNull
    @Override
    public Iterator<T> iterator() {
        return super.values().iterator();
    }
    @Override
    public void forEach(Consumer<? super T> action) {
        super.values().forEach(action);
    }
    @Override
    public Spliterator<T> spliterator() {
        return super.values().spliterator();
    }

}
