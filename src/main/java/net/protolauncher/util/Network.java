package net.protolauncher.util;

import javax.net.ssl.HttpsURLConnection;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Inet4Address;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class Network {

    // Suppress default constructor
    private Network() { }

    // User Agent
    private static final String USER_AGENT = "ProtoLauncher/1.0";

    /**
     * Creates a request to the specified URL using the specified method whilst handling JSON content.
     *
     * @param endpoint The endpoint you wish to connect to
     * @param method What method to use when connecting to the endpoint
     * @param json Whether or not to include the application/json Content-Type and Accept headers
     * @return A new HttpsURLConnection
     * @throws IOException Thrown if creating the connection fails
     */
    public static HttpsURLConnection createConnection(URL endpoint, String method, boolean json) throws IOException {
        // Open a new connection and set the method and user-agent
        HttpsURLConnection connection = (HttpsURLConnection) endpoint.openConnection();
        connection.setRequestMethod(method);
        connection.setRequestProperty("User-Agent", USER_AGENT);

        // If JSON, then set content-type and accept headers
        if (json) {
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
        }

        // Enable reading responses & return connection
        connection.setDoOutput(true);
        return connection;
    }

    /**
     * Creates a request to the specified URL using the specified method assuming JSON content.
     *
     * @param endpoint The endpoint you wish to connect to
     * @param method What method to use when connecting to the endpoint
     * @return A new HttpsURLConnection
     * @throws IOException Thrown if creating the connection fails
     * @see Network#createConnection(URL, String, boolean)
     */
    public static HttpsURLConnection createConnection(URL endpoint, String method) throws IOException {
        return createConnection(endpoint, method, true);
    }

    /**
     * Creates a request to the specified URL assuming a POST method and JSON content.
     *
     * @param endpoint The endpoint you wish to connect to
     * @return A new HttpsURLConnection
     * @throws IOException Thrown if creating the connection fails
     * @see Network#createConnection(URL, String, boolean)
     */
    public static HttpsURLConnection createConnection(URL endpoint) throws IOException {
        return createConnection(endpoint, "POST", true);
    }

    /**
     * Sends a request using the specified connection.
     *
     * @param connection The connection to use for the request
     * @param safe Whether or not to throw an exception on an errored HTTP response code
     * @return The {@link HttpsURLConnection#getInputStream()} or the {@link HttpsURLConnection#getErrorStream()}
     * @throws IOException Thrown if there is an issue making the request
     */
    public static InputStream send(HttpsURLConnection connection, boolean safe) throws IOException {
        // Send the request
        int code = connection.getResponseCode();

        // Handle "safe mode"
        if (safe) {
            if (code >= 400 && code < 500) {
                throw new IOException(code + " Bad Request");
            } else if (code >= 500) {
                throw new IOException(code + " Internal Server Error");
            }
        }

        // Handle "safe" sending and determine the correct stream
        InputStream stream;
        if (code >= 400) {
            stream = connection.getErrorStream();
        } else {
            stream = connection.getInputStream();
        }

        // Return the stream
        return stream;
    }

    /**
     * Sends a request using the specified connection while checking for a valid response code.
     *
     * @param connection The connection to use for the request
     * @return The {@link HttpsURLConnection#getInputStream()} or the {@link HttpsURLConnection#getErrorStream()}
     * @throws IOException Thrown if there is an issue making the request
     * @see Network#send(HttpsURLConnection, boolean)
     */
    public static InputStream send(HttpsURLConnection connection) throws IOException {
        return send(connection, true);
    }

    /**
     * Creates a connection using the specified URL and subsequently sends the connection returning the responding stream.
     * Useful for requesting a file without downloading it.<br><br>
     * Note: This uses the *safe* send method. Errored streams will throw exceptions.
     *
     * @param endpoint The endpoint to fetch the stream from
     * @return The responding stream
     * @throws IOException Thrown if there is an error fetching the stream
     * @see Network#createConnection(URL, String, boolean)
     * @see Network#send(HttpsURLConnection, boolean)
     */
    public static InputStream fetch(URL endpoint) throws IOException {
        return send(createConnection(endpoint, "GET", false), true);
    }

    /**
     * Fetches a file from the specified endpoint and subsequently copies the returning stream to the specified path.
     * This method will replace an existing file.
     *
     * @param endpoint The endpoint to fetch the stream from
     * @param location The location in which the file should be written to
     * @throws IOException Thrown if there is an error downloading the file
     * @see Network#fetch(URL)
     */
    public static void download(URL endpoint, Path location) throws IOException {
        Files.copy(fetch(endpoint), location, StandardCopyOption.REPLACE_EXISTING);
    }

    // The internal function for isConnected. Not publically accessible because modifying attempts provides no good functionality.
    private static boolean isConnected(int timeout, Inet4Address address, int attempts) {
        try {
            return address.isReachable(timeout);
        } catch (IOException e) {
            if (attempts >= 5) {
                return false;
            } else {
                return isConnected(timeout, address, ++attempts);
            }
        }
    }

    /**
     * Attempts to determine if there is a connection to the internet using the specified timeout and the specified host retrying 5 times.<br><br>
     * Note: This method is *blocking* and may take a long time to return.
     *
     * @param timeout The time, in milliseconds, to wait between attempts. See {@link Inet4Address#isReachable(int)}
     * @param address The {@link Inet4Address} to use for attempting connections
     * @return Whether or not we assume we are connected to the internet
     */
    private static boolean isConnected(int timeout, Inet4Address address) {
        return isConnected(timeout, address, 0);
    }

    /**
     * Attempts to determine if there is a connection to the internet using the specified timeout and using the address 1.1.1.1 retrying 5 times.<br><br>
     * Note: This method is *blocking* and may take a long time to return.
     *
     * @param timeout The time, in milliseconds, to wait between attempts. See {@link Inet4Address#isReachable(int)}
     * @return Whether or not we assume we are connected to the internet
     */
    public static boolean isConnected(int timeout) {
        try {
            return isConnected(timeout, (Inet4Address) Inet4Address.getByAddress(new byte[]{ 1, 1, 1, 1 }));
        } catch (UnknownHostException e) {
            // If this happens then somebody screwed up.
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Attempts to determine if there is a connection to the internet using a retry timeout of 250 milliseconds using address 1.1.1.1 retrying 5 times.<br><br>
     * Note: This method is *blocking* and may take a long time to return.
     *
     * @return Whether or not we assume we are connected to the internet
     */
    public static boolean isConnected() {
        return isConnected(250);
    }

    // Parses a connection's input stream and returns it as a string
    // I barely have any idea how this works, I took it from StackOverflow

    /**
     * Converts a connection's InputStream to a string
     *
     * @param stream The input stream
     * @return The string
     * @throws IOException Thrown if there is an error reading the stream
     */
    public static String stringify(InputStream stream) throws IOException {
        ByteArrayOutputStream string = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = stream.read(buffer)) != -1) {
            string.write(buffer, 0, length);
        }
        return string.toString(StandardCharsets.UTF_8);
    }

}
