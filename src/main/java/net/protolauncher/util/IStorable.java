package net.protolauncher.util;

public interface IStorable {

    String getUuid();

}
