package net.protolauncher.util;

import com.google.gson.Gson;
import com.google.gson.InstanceCreator;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public interface ISaveable {

    Type getType();
    Gson getGson();
    Path getLocation();

    /**
     * Saves this object.
     *
     * @param <T> The extended object to cast to.
     * @return The object after having been saved.
     * @throws IOException Thrown if there was an exception while trying to save.
     */
    @SuppressWarnings("unchecked")
    default <T extends ISaveable> T save() throws IOException {
        Path location = getLocation().toAbsolutePath();
        if (location.getParent() != null) {
            Files.createDirectories(location.getParent());
        }
        Files.write(getLocation(), getGson().toJson(this).getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING, LinkOption.NOFOLLOW_LINKS);
        return (T) this;
    }

    /**
     * Loads this object.
     *
     * @param <T> The extended object to cast to.
     * @return The object after loading the new values.
     * @throws IOException Thrown if there was an exception while trying to load.
     */
    @SuppressWarnings("unchecked")
    default <T extends ISaveable> T load() throws IOException {
        if (!Files.exists(getLocation())) {
            return save();
        } else {
            InstanceCreator<?> creator = type -> (T) this;
            Gson newGson = getGson().newBuilder().registerTypeAdapter(getType(), creator).create();
            return newGson.fromJson(Files.newBufferedReader(getLocation()), getType());
        }
    }

}
